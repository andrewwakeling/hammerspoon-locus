# hs.locus

This modules provides a different approach to layout of windows. In particular, it attempts resolve the undesirable way [OSX switches focus between applications instead of windows](http://blog.wuwon.id.au/2012/06/trouble-with-os-x-window-manager.html).

Locus solves the focus issue in a very particular way, by using juggling windows between spaces/desktops. Locus also assumes that windows described in a layout will appear in a clean space. This means that any existing windows not described in the layout, will not be shown.

**Please Note**: If you are already using spaces/desktops to manage windows, then it's very likely that Locus will mess with your mental model.

# Installation

## Dependencies
This module depends on [hs._asm.undocumented.spaces](https://github.com/asmagill/hs._asm.undocumented.spaces). Please ensure that it is installed before running Locus.


## Install Locus
Clone this repository, then create a symlink to it:
	
	git clone https://bitbucket.org/andrewwakeling/hammerspoon-locus
	cd hammerspoon-locus
	ln -s "$PWD" ~/.hammerspoon/hammerspoon-locus
	
Sample usage:

	local locus = require('hammerspoon-locus')
	hs.hotkey.bind(HYPER, "C", function()
		local layouts = {}
		table.insert(layouts, {
			window = locus.findExact("Google Chrome", "")[1], 
			layout = hs.layout.maximized,
			focus = true
		})
		locus.apply(layouts, hs.screen.mainScreen())
	end)
	
## API

### locus.find(fn)
Return all windows (from all spaces) that match the given predicate function.

**Note:** hs.window.filter and some other methods may not find reliably retrieve the desired window as windows, as it appears that juggling windows causes reliability issues. Please use this method (or locus.findExact) when using Locus.

### locus.findExact(app, title)
Return all windows (from all spaces) that match the specified app name and title name.


### locus.apply(table, screen)
Using the specified table, apply the layout.

Each table element consists of:

- "window": an instance of hs.window
- "layout": a layout value which behaves in accordance with [hs.window.moveToUnit](http://www.hammerspoon.org/docs/hs.window.html#moveToUnit) (See also [constants from ls.layout](http://www.hammerspoon.org/docs/hs.layout.html))
- "focus": a boolean indicating that this window should receive focus. Only 1 window will receive focus

# Other References

#### Liberate your caps-lock key
Use [Seil](https://pqrs.org/osx/karabiner/seil.html.en) to map your caps-lock key to F19 and then using [Karabiner](https://pqrs.org/osx/karabiner/), turn it into a "hyper" key (a combination of shift, alt, control & command).

#### Keep frequenty used websites in their own app
Tired of searching through tabs for your email or another website? Try [Epichrome](https://github.com/dmarmor/epichrome/). Together with Locus, these apps can be one shortcut key away!

### Known Issues

- switching to new layouts rapidly may result in some flickering if the windows do not occupy all of the screen