--- === hs.locus ===

local _ = hs.fnutils

local spaces = require("hs._asm.undocumented.spaces")

local module = {}

local function currentSpace(screen)
    local currentSpaces = spaces.query(spaces.masks.currentSpaces)
    return _.filter(screen:spaces(), function(space)
        return _.contains(currentSpaces, space)
    end)[1]
end

local function nonCurrentSpaces(screen)
    local currentSpaces = spaces.query(spaces.masks.currentSpaces)
    return _.filter(screen:spaces(), function(space)
        return not _.contains(currentSpaces, space)
    end)
end

module.find = function(fn)
    local windows = {}
    _.each(spaces.query(), function(space)
        windows = _.concat(windows, _.filter(spaces.allWindowsForSpace(space), fn))
    end)
    return windows
end

module.findExact = function(app, title)
    return module.find(function(window)
        return window:application():name():find(app, 1, true) ~= nil and window:title():find(title, 1, true) ~= nil and window:isStandard()
    end)
end

module.apply = function(table, screen)
    local nonCurrent = nonCurrentSpaces(screen)

    if (#nonCurrent >= 2) then
        local prepareSpace = nonCurrent[1]
        local junkSpace = nonCurrent[2]
        local focusWindow = nil

        _.each(spaces.allWindowsForSpace(prepareSpace), function(window)
            if (window:isStandard()) then
                spaces.moveWindowToSpace(window:id(), junkSpace)
            end
        end)

        _.each(table, function(entry)
            if (entry.window) then
                spaces.moveWindowToSpace(entry.window:id(), prepareSpace)
                entry.window:moveToUnit(entry.layout, 0)
                if (entry.focus) then
                    focusWindow = entry.window
                end
            end
        end)

        spaces.changeToSpace(prepareSpace, true)
        if (focusWindow) then
            focusWindow:focus()
        end
    else
        hs.alert("You must have at least 3 spaces on the specified screen to use locus.")
    end
end

return module